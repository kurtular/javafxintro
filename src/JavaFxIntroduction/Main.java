package JavaFxIntroduction;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Main extends Application {

    public static void main(String[] args) {
	launch(args);
    }

    @Override
    public void start(Stage stage){
        stage.setTitle("Login");
        GridPane grid =new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(30);
        grid.setPadding(new Insets(25, 25, 25, 25));
        Scene scene = new Scene(grid,300,275);

        Text sceneTitle=new Text("Please Login");

        Label userName = new Label("UserName");
        TextField userTextField = new TextField();

        Label password = new Label("Password");
        PasswordField passwordField = new PasswordField();

        grid.add(sceneTitle, 1, 0);
        grid.add(userName, 0, 1);
        grid.add(userTextField, 1, 1);
        grid.add(password, 0, 2);
        grid.add(passwordField, 1, 2);

        scene.getStylesheets().add("Login.css");
        stage.setScene(scene);
        stage.show();
    }
}
